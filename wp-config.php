<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'vtracking' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'XxI4Je$dv)m;qht#X[2EujqJ<tz><6zSu#|gg3d/y^/HMjJ.pqUk,G~ulX_h/Q2V' );
define( 'SECURE_AUTH_KEY',  'E.w<i0#!sMH<rRZ;!.DrT[ux>2|[]QXU@*Rf[ZmkpDP@XANy!ws<>uKbOXPP>ZZ6' );
define( 'LOGGED_IN_KEY',    'xbzky9ji7r~V>2p[Pb4R7[y+;]Dws{_&90mZRHOWx j_=id-)nCmP,M}KKQ6 >kC' );
define( 'NONCE_KEY',        ';pLft 2g9!Ke+F^6^_yWCPvnq$`1VvN)N0CRX<_C;,lP#gDF?22sSh8.inl/K *F' );
define( 'AUTH_SALT',        '%ZQ>}Q/y#0~o9{],x#xI)Ui$ZjL~)[Tj;{cx G(uO#=M`q.UZXP@YTTj[#5+<%ay' );
define( 'SECURE_AUTH_SALT', 'Mn8Xe^Y[sQOZ/%}s,,t8[ZA[4@B_]NEk,P&Y^UH}4b#>7`1tQ*--vN&TWj_|{.Gh' );
define( 'LOGGED_IN_SALT',   'YUa]a1LcEy5NNAHLPdQ:i?]J!S6.Xd@-O)V>Q1qGyrLIpqs9n7rYP<nu`jY6O(/H' );
define( 'NONCE_SALT',       'V|*Iej?>0Qm6M~>aO4y.!x`;XBAZeYx3F{)34F*3@(G=ND7FtXxFu)CXFBQXl~jF' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
