﻿<?php
get_header();
?>
<main class="main-site">
      <!--Section Slider-->
      <section class="section section-slider">
        <div class="container">
          <div class="row">
            <!--Slider Wrap-->
            <div class="slider__wrap">
              <div class="col-xs-12 col-md-8 slider__left">
                <h2 class="wow fadeInUp" animation-duration="1.5s">Hệ thông giám sát hành trình SmartBike</h2>
                <p class="wow fadeInUp" animation-duration="1.5s" data-animated-delay="0.3s">Chỉ cần truy cập hệ thống từ các thiết bị có kết nối internet để giám sát hành trình và quản lý những chiếc xe của bạn</p>
                <div class="slider__left__btn wow fadeInUp" animation-duration="1.5s" data-animated-delay="0.6s">
                  <a href="http://vtracking.viettel.vn">ĐĂNG KÝ TRẢI NGHIỆM NGAY</a>
                </div>
              </div>
            </div>
            <!--End Slider Wrap-->
          </div>
        </div>
        <div class="col-xs-12 col-md-6 wrap-images-header item-img-slider">
          <img class="wow fadeInRight item-images-slider " animation-duration="1.5s" data-animated-delay="0.5s" src="<?php echo get_template_directory_uri(); ?>/images/photos/photo-banner.svg" alt="">
        </div>
      </section>
      <!--End Section Slider-->

      <!-- block giới thiệu slider -->
      <section id="tin-nang" class="section section-about">
        <div class="container">
          <div class="row">
            <div class="wow fadeInUp item item-wrap-slider" animation-duration="1.5s" data-animated-delay="0.3s">
              <div class="function-wrap">
                <div class="col-xs-12 section-content hg-section-content">
                  <summary class="item-summary">Chức năng của SmartBike</summary>
                  <h2 class="title-about-tracking">Định vị vị trí nhân viên mọi lúc, mọi nơi</h2>
                  <p class="des-about">SmartBike là dịch vụ cho phép quản lý, theo dõi hành trình, giám sát các phương tiện vận tải dựa trên ứng dụng công nghệ định vị vệ tinh GPS kết hợp với hệ thống mạng viễn thông di động Viettel.</p>
                  <p class="des-about">Thiết bị giám sát hành trình là thiết bị định vị qua GPS và truyền tin qua GPRS. Thiết bị này được lắp trên phương tiện cần giám sát và tự động truyền thông số về trung tâm sau mỗi khoảng thời gian
                    cố định cho phép. Thiết bị có tích hợp khe cắm thẻ nhớ, khe cắm sim.</p>
                </div>
                <div class="col-xs-12 section-photo">
                  <img class="item-thin-images" src="<?php echo get_template_directory_uri(); ?>/images/photos/photo-function.png" alt="">
                </div>
              </div>
              <div class="function-wrap">
                <div class="col-xs-12 section-content hg-section-content">
                  <summary class="item-summary">Chức năng của SmartBike 2</summary>
                  <h2 class="title-about-tracking">Định vị vị trí nhân viên mọi lúc, mọi nơi</h2>
                  <p class="des-about">SmartBike là dịch vụ cho phép quản lý, theo dõi hành trình, giám sát các phương tiện vận tải dựa trên ứng dụng công nghệ định vị vệ tinh GPS kết hợp với hệ thống mạng viễn thông di động Viettel.</p>
                  <p class="des-about">Thiết bị giám sát hành trình là thiết bị định vị qua GPS và truyền tin qua GPRS. Thiết bị này được lắp trên phương tiện cần giám sát và tự động truyền thông số về trung tâm sau mỗi khoảng thời gian
                    cố định cho phép. Thiết bị có tích hợp khe cắm thẻ nhớ, khe cắm sim.</p>
                </div>
                <div class="col-xs-12 section-photo">
                  <img class="item-thin-images" src="<?php echo get_template_directory_uri(); ?>/images/photos/photo-function.png" alt="">
                </div>
              </div>
              <div class="function-wrap">
                <div class="col-xs-12 section-content hg-section-content">
                  <summary class="item-summary">Chức năng của SmartBike 3</summary>
                  <h2 class="title-about-tracking">Định vị vị trí nhân viên mọi lúc, mọi nơi</h2>
                  <p class="des-about">SmartBike là dịch vụ cho phép quản lý, theo dõi hành trình, giám sát các phương tiện vận tải dựa trên ứng dụng công nghệ định vị vệ tinh GPS kết hợp với hệ thống mạng viễn thông di động Viettel.</p>
                  <p class="des-about">Thiết bị giám sát hành trình là thiết bị định vị qua GPS và truyền tin qua GPRS. Thiết bị này được lắp trên phương tiện cần giám sát và tự động truyền thông số về trung tâm sau mỗi khoảng thời gian
                    cố định cho phép. Thiết bị có tích hợp khe cắm thẻ nhớ, khe cắm sim.</p>
                </div>
                <div class="col-xs-12 section-photo">
                  <img class="item-thin-images" src="<?php echo get_template_directory_uri(); ?>/images/photos/photo-function.png" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- end block giới thiệu slider -->
      <div class="wrap-section-about">
        <div class="item-bg"></div>
        <section id="mo-hinh" class="section section-about section-model ">
          <div class="container">
            <div class="row">
              <div class="item">
                <div class="function-wrap">
                  <div class="col-xs-12 section-content hg-section-content wow fadeInRight" animation-duration="1.5s">
                    <h2 class="title-about-tracking title-heading-mohinh">Mô hình hệ thống SmartBike</h2>
                    <div class="item-caption">
                      <div class="wrap-image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/images/icon-database.svg" alt="">
                      </div>
                      <div class="wrap-content">
                        <h4 class="item-title">Hệ thống database server</h4>
                        <p class="item-descreption">Hệ thống Database server: Ghi nhận các thông số truyền về từ thiết bị gắn trên phương tiện, phân tích xử lý, tổng hợp dữ liệu để đưa ra các báo cáo thống kê giúp nhà điều hành quản lý đưa ra
                          quyết định tốt hơn cho doanh nghiệp của mình.</p>
                      </div>
                    </div>
                    <div class="item-caption">
                      <div class="wrap-image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/images/icon-map.svg" alt="">
                      </div>
                      <div class="wrap-content">
                        <h4 class="item-title">Hệ thống Map server</h4>
                        <p class="item-descreption">Hệ thống Map server: Cho phép người dùng có thể xem được các vị trí, vận tốc, hướng di chuyển và các thông số hiện tại của phương tiện giám sát.</p>
                      </div>
                    </div>
                    <div class="item-caption">
                      <div class="wrap-image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/images/icon-server.svg" alt="">
                      </div>
                      <div class="wrap-content">
                        <h4 class="item-title">Hệ thống Web server</h4>
                        <p class="item-descreption">Hệ thống Web server: Cung cấp giao diện quản lý dịch vụ, mỗi khách hàng sẽ được cung cấp một tài khoản kết nối đến web server thông qua trình duyệt web để quản lý, giám sát phương tiện của mình.</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 section-photo wow fadeInUp" animation-duration="1.5s">
                    <img class="item-thin-images" src="<?php echo get_template_directory_uri(); ?>/images/photos/photo-model.svg" alt="">
                  </div>
                </div>
                <div class="carousel-caption"></div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <section id="phuong-thuc" class="section section-about section-method">
        <div class="container">
          <div class="wrap-heading-method">
            <h2 class="item-title-heading wow fadeInUp" animation-duration="1s">Phương thức hoạt động</h2>
            <p class="item-des-heading wow fadeInUp" animation-duration="1s">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo</p>
          </div>
          <div class="row item-wrap-content">
            <div class="col-xs-12 col-md-4 wrap-content tab-wrap-content wow zoomIn" animation-duration="1s">
              <div class="wrap-wh-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/photos/photo-gps.svg" class="item-img" alt="">
              </div>
              <div class="wrap-content-phuongthuc">
                <h4 class="item-title">Định vị thông qua tọa độ GPS</h4>
                <p class="item-descreption">Thiết bị định vị hành trình sẽ cung cấp tính năng định vị thông qua tọa độ GPS. Toàn bộ thông tin của tọa độ về GPS của phương tiện sẽ được truyền tải qua sóng GPRS của Viettel về trung tâm điều hành.</p>
              </div>
            </div>
            <div class="col-xs-12 col-md-4 wrap-content tab-wrap-content wow zoomIn" animation-duration="1s" data-animated-delay="0.1s">
              <div class="wrap-wh-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/photos/photo-database.svg" class="item-img" alt="">
              </div>
              <div class="wrap-content-phuongthuc">
                <h4 class="item-title">Dữ liệu từ Database & Webserver</h4>
                <p class="item-descreption">Trên hệ thống Server, toàn bộ các thông tin về tọa độ của phương tiện sẽ được lưu trữ tại Database server. Web server có nhiệm vụ tổng hợp và hiển thị vị trí tương ứng của xe trên bản đồ.</p>
              </div>
            </div>
            <div class="col-xs-12 col-md-4 wrap-content tab-wrap-content wow zoomIn" animation-duration="1s" data-animated-delay="0.1s">
              <div class="wrap-wh-img">
                <img src="<?php echo get_template_directory_uri(); ?>/images/photos/photo-monitoring.svg" class="item-img" alt="">
              </div>
              <div class="wrap-content-phuongthuc">
                <h4 class="item-title">Giám sát phương tiện</h4>
                <p class="item-descreption">Khách hàng truy cập vào trình duyệt web và liên kết với web server để giám sát phương tiện của mình kèm theo cảnh báo, cảm ứng trạng thái và tọa độ của xe từ Database server.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="doi_tuong" class="section section-about section-member">
        <div class="item-section-member">
          <div class="container">
            <div class="item-heading-member">
              <h2 class="item-title-heading">Các đối tượng áp dụng theo Nghị định </h2>
              <p class="item-des-heading">Sed ut perspiciatis unde omnis iste natus error sit volup tatem accusantium doloremque </p>
            </div>
            <div class="row mr-top-60">
              <div class="con-xs-12 col-sm-6 pd-13 wow zoomIn" animation-duration="1s" data-animated-delay="0.1s">
                <div class="item-caption">
                  <div class="wrap-image">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/images/icon-object.svg" alt="">
                  </div>
                  <div class="wrap-content">
                    <h4 class="item-title">Các đối tượng áp dụng theo Nghị định 86/2014/NĐ-CP</h4>
                    <p class="item-descreption">Sed ut perspiciatis unde omnis iste natus error sit volup tatem accusantium doloremque </p>
                  </div>
                </div>
              </div>
              <div class="con-xs-12 col-sm-6 pd-13 wow zoomIn" animation-duration="1s" data-animated-delay="0.2s">
                <div class="item-caption">
                  <div class="wrap-image">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/images/icon-enterprise.svg" alt="">
                  </div>
                  <div class="wrap-content">
                    <h4 class="item-title">Doanh nghiệp cần quản lý hành trình, giám sát nhiên liệu phương tiện</h4>
                    <p class="item-descreption">Sed ut perspiciatis unde omnis iste natus error sit volup tatem accusantium doloremque</p>
                  </div>
                </div>
              </div>
              <div class="con-xs-12 col-sm-6 pd-13 wow zoomIn" animation-duration="1s" data-animated-delay="0.3s">
                <div class="item-caption">
                  <div class="wrap-image">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/images/icon-taxi.svg" alt="">
                  </div>
                  <div class="wrap-content">
                    <h4 class="item-title">Quản lý và giám sát xe Taxi</h4>
                    <p class="item-descreption">Sed ut perspiciatis unde omnis iste natus error sit volup tatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo</p>
                  </div>
                </div>
              </div>
              <div class="con-xs-12 col-sm-6 pd-13 wow zoomIn" animation-duration="1s" data-animated-delay="0.4s">
                <div class="item-caption">
                  <div class="wrap-image">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/images/icon-wifi.svg" alt="">
                  </div>
                  <div class="wrap-content">
                    <h4 class="item-title">Cá nhân muốn giám sát hành trình xe của mình</h4>
                    <p class="item-descreption">Sed ut perspiciatis unde omnis iste natus error sit volup tatem accusantium doloremque</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item-customer">
          <div class="item-bg"></div>
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-md-4 tab-mg-bt30 wow fadeInLeft" animation-duration="1.5s">
                <h2 class="item-title-heading">Khách hàng của <br> SmartBike</h2>
                <p class="item-des-heading">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem</p>
              </div>
              <div class="con-xs-12 col-sm-6 col-md-4">
                <div class="item-caption">
                  <div class="wrap-image">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/images/icon-group.svg" alt="">
                  </div>
                  <div class="wrap-content">
                    <h4 class="item-title counter" data-count="2300">0</h4>
                    <p class="item-descreption">Tổng số khách hàng đang sử dụng dịch vụ</p>
                  </div>
                </div>
              </div>
              <div class="con-xs-12 col-sm-6 col-md-4">
                <div class="item-caption">
                  <div class="wrap-image">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/images/icon-sim.svg" alt="">
                  </div>
                  <div class="wrap-content">
                    <h4 class="item-title counter" data-count="48000">0</h4>
                    <p class="item-descreption">Tổng số thuê bao đã kích hoạt dịch vụ</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row item-wrap-logo">
              <div class="item-client-img wow zoomIn" animation-duration="1.5s" data-animated-delay="0.1.5s">
                <img class="client-img" src="<?php echo get_template_directory_uri(); ?>/images/images/logo-client-1.png" alt="">
              </div>
              <div class="item-client-img wow zoomIn" animation-duration="1.5s" data-animated-delay="0.2s">
                <img class="client-img" src="<?php echo get_template_directory_uri(); ?>/images/images/logo-client-2.png" alt="">
              </div>
              <div class="item-client-img wow zoomIn" animation-duration="1.5s" data-animated-delay="0.3s">
                <img class="client-img" src="<?php echo get_template_directory_uri(); ?>/images/images/logo-client-3.png" alt="">
              </div>
              <div class="item-client-img wow zoomIn" animation-duration="1.5s" data-animated-delay="0.4s">
                <img class="client-img" src="<?php echo get_template_directory_uri(); ?>/images/images/logo-client-4.png" alt="">
              </div>
              <div class="item-client-img wow zoomIn" animation-duration="1.5s" data-animated-delay="0.5s">
                <img class="client-img" src="<?php echo get_template_directory_uri(); ?>/images/images/logo-client-5.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="dich-vu" class="section section-about section-member">
        <div class="container">
          <div class="item-heading-member">
            <h2 class="item-title-heading">Phí dịch vụ</h2>
            <p class="item-des-heading">Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p>
          </div>
          <div class="item-tab-table">
            <div class="tab wow fadeInUp" animation-duration="1.5s">
              <button class="tablinks active" onclick="openCity(event, 'tabOne')" id="defaultOpen">Triển khai kinh doanh các gói cước không kèm thiết bị</button>
              <button class="tablinks" onclick="openCity(event, 'tabTwo')">Triển khai kinh doanh các gói cước không kèm thiết bị</button>
            </div>
            <div id="tabOne" class="tabcontent wow zoomIn" animation-duration="1.5s">
              <table class="item-table">
                <tr>
                  <th class="item-title-table">Loại hình</th>
                  <th class="item-title-table">Mã gói</th>
                  <th class="item-title-table">Chu kì đóng cước</th>
                  <th class="item-title-table">Phí hòa mạng (VND)</th>
                  <th class="item-title-table">Phí cước hàng tháng (VND)</th>
                  <th class="item-title-table">Tổng cước đóng trước</th>
                  <th class="item-title-table">Tổng số tiền khách hàng phải đóng tại thời điểm triển khai (VNĐ)</th>
                </tr>
                <tr>
                  <td class="item-content-table">Trả sau</td>
                  <td class="item-content-table">DB80_KTB</td>
                  <td class="item-content-table">Hàng tháng</td>
                  <td class="item-content-table">50.000</td>
                  <td class="item-content-table">80.000</td>
                  <td class="item-content-table">0</td>
                  <td class="item-content-table">50.000</td>
                </tr>
                <tr>
                  <td class="item-content-table">Trả sau</td>
                  <td class="item-content-table">DB80_KTB</td>
                  <td class="item-content-table">Hàng tháng</td>
                  <td class="item-content-table">50.000</td>
                  <td class="item-content-table">80.000</td>
                  <td class="item-content-table">0</td>
                  <td class="item-content-table">50.000</td>
                </tr>
                <tr>
                  <td class="item-content-table">Trả sau</td>
                  <td class="item-content-table">DB80_KTB</td>
                  <td class="item-content-table">Hàng tháng</td>
                  <td class="item-content-table">50.000</td>
                  <td class="item-content-table">80.000</td>
                  <td class="item-content-table">0</td>
                  <td class="item-content-table">50.000</td>
                </tr>
                <tr>
                  <td class="item-content-table">Trả sau</td>
                  <td class="item-content-table">DB80_KTB</td>
                  <td class="item-content-table">Hàng tháng</td>
                  <td class="item-content-table">50.000</td>
                  <td class="item-content-table">80.000</td>
                  <td class="item-content-table">0</td>
                  <td class="item-content-table">50.000</td>
                </tr>
              </table>
            </div>

            <div id="tabTwo" class="tabcontent wow zoomIn" style="display: none" animation-duration="1.5s">
              <table class="item-table">
                <tr>
                  <th class="item-title-table">Loại hình 1</th>
                  <th class="item-title-table">Mã gói</th>
                  <th class="item-title-table">Chu kì đóng cước</th>
                  <th class="item-title-table">Phí hòa mạng (VND)</th>
                  <th class="item-title-table">Phí cước hàng tháng (VND)</th>
                  <th class="item-title-table">Tổng cước đóng trước</th>
                  <th class="item-title-table">Tổng số tiền khách hàng phải đóng tại thời điểm triển khai (VNĐ)</th>
                </tr>
                <tr>
                  <td class="item-content-table">Trả sau</td>
                  <td class="item-content-table">DB80_KTB</td>
                  <td class="item-content-table">Hàng tháng</td>
                  <td class="item-content-table">50.000</td>
                  <td class="item-content-table">80.000</td>
                  <td class="item-content-table">0</td>
                  <td class="item-content-table">50.000</td>
                </tr>
                <tr>
                  <td class="item-content-table">Trả sau</td>
                  <td class="item-content-table">DB80_KTB</td>
                  <td class="item-content-table">Hàng tháng</td>
                  <td class="item-content-table">50.000</td>
                  <td class="item-content-table">80.000</td>
                  <td class="item-content-table">0</td>
                  <td class="item-content-table">50.000</td>
                </tr>
                <tr>
                  <td class="item-content-table">Trả sau</td>
                  <td class="item-content-table">DB80_KTB</td>
                  <td class="item-content-table">Hàng tháng</td>
                  <td class="item-content-table">50.000</td>
                  <td class="item-content-table">80.000</td>
                  <td class="item-content-table">0</td>
                  <td class="item-content-table">50.000</td>
                </tr>
                <tr>
                  <td class="item-content-table">Trả sau</td>
                  <td class="item-content-table">DB80_KTB</td>
                  <td class="item-content-table">Hàng tháng</td>
                  <td class="item-content-table">50.000</td>
                  <td class="item-content-table">80.000</td>
                  <td class="item-content-table">0</td>
                  <td class="item-content-table">50.000</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </section>
      <section id="dai-ly" class="section section-about section-member section-daily">
        <div class="item-bg"></div>
        <div class="container">
          <div class="item-heading-member wow fadeInUp" animation-duration="1.5s">
            <h2 class="item-title-heading">Đại lý</h2>
            <p class="item-des-heading">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo</p>
          </div>
          <div class="item-content-daily">
            <div class="item-select">
              <div class="wrap-icon-select">
                <!-- <select class="item-wrap-select" name="" id="">
                    <i class="fa fa-chevron-down"></i>
                    <option value="">Đà nẵng</option>
                    <option value="">An Giang</option>
                    <option value="">Quảng Bình</option>
                  </select> -->
                <select class="item-wrap-select" id="e1">
                    <option value="AL">Đà Nẵng</option>
                    <option value="WY">Hà Nội</option>
                    <option value="AL">Yên Bái</option>
                    <option value="WY">Vĩnh Phúc</option>
                    <option value="AL">Vĩnh Long</option>
                    <option value="WY">Trà Vinh</option>
                    <option value="AL">Tiền Giang</option>
                    <option value="WY">Thừa Thiên - Huế</option>
                    <option value="AL">Thái Nguyên</option>
                    <option value="WY">Thanh Hóa</option>
                    <option value="AL">Sóc Trăng</option>
                    <option value="WY">Quảng Trị</option>
                    <option value="AL">Quảng Ninh</option>
                    <option value="WY">Quảng Nam</option>
                    <option value="AL">Quảng Bình</option>
                    <option value="WY">Phú Yên</option>
                    <option value="AL">Phú Thọ</option>
                    <option value="WY">Ninh Bình</option>
                  </select>
              </div>
            </div>
            <div class="wrap-daily">
              <ul class="col-xs-12 col-sm-6 item-wrap-list wow zoomIn" animation-duration="1.5s" data-animated-delay="0">
                <li class="list-wrap-li">
                  <h4 class="item-title">1. Chi nhánh Công ty TNHH một thành viên thương mại dịch vụ Quốc Thái 2</h4>
                  <p class="item-descreption">Đường Khu Nhà 03 Căn - Khóm Thới Hòa - Nhà Bàn - Tịnh Biên - An Giang</p>
                  <p class="item-descreption">0939 631 907</p>
                </li>
              </ul>
              <ul class="col-xs-12 col-sm-6 item-wrap-list wow zoomIn" animation-duration="1.5s" data-animated-delay="0.1">
                <li class="list-wrap-li">
                  <h4 class="item-title">1. Chi nhánh Công ty TNHH một thành viên thương mại dịch vụ Quốc Thái 2</h4>
                  <p class="item-descreption">Đường Khu Nhà 03 Căn - Khóm Thới Hòa - Nhà Bàn - Tịnh Biên - An Giang</p>
                  <p class="item-descreption">0939 631 907</p>
                </li>
              </ul>
              <ul class="col-xs-12 col-sm-6 item-wrap-list wow zoomIn" animation-duration="1.5s" data-animated-delay="0.2">
                <li class="list-wrap-li">
                  <h4 class="item-title">1. Chi nhánh Công ty TNHH một thành viên thương mại dịch vụ Quốc Thái 2</h4>
                  <p class="item-descreption">Đường Khu Nhà 03 Căn - Khóm Thới Hòa - Nhà Bàn - Tịnh Biên - An Giang</p>
                  <p class="item-descreption">0939 631 907</p>
                </li>
              </ul>
              <ul class="col-xs-12 col-sm-6 item-wrap-list wow zoomIn" animation-duration="1.5s" data-animated-delay="0.3">
                <li class="list-wrap-li">
                  <h4 class="item-title">1. Chi nhánh Công ty TNHH một thành viên thương mại dịch vụ Quốc Thái 2</h4>
                  <p class="item-descreption">Đường Khu Nhà 03 Căn - Khóm Thới Hòa - Nhà Bàn - Tịnh Biên - An Giang</p>
                  <p class="item-descreption">0939 631 907</p>
                </li>
              </ul>
              <ul class="col-xs-12 col-sm-6 item-wrap-list wow zoomIn" animation-duration="1.5s" data-animated-delay="0.4">
                <li class="list-wrap-li">
                  <h4 class="item-title">1. Chi nhánh Công ty TNHH một thành viên thương mại dịch vụ Quốc Thái 2</h4>
                  <p class="item-descreption">Đường Khu Nhà 03 Căn - Khóm Thới Hòa - Nhà Bàn - Tịnh Biên - An Giang</p>
                  <p class="item-descreption">0939 631 907</p>
                </li>
              </ul>
              <ul class="col-xs-12 col-sm-6 item-wrap-list wow zoomIn" animation-duration="1.5s" data-animated-delay="0.5">
                <li class="list-wrap-li">
                  <h4 class="item-title">1. Chi nhánh Công ty TNHH một thành viên thương mại dịch vụ Quốc Thái 2</h4>
                  <p class="item-descreption">Đường Khu Nhà 03 Căn - Khóm Thới Hòa - Nhà Bàn - Tịnh Biên - An Giang</p>
                  <p class="item-descreption">0939 631 907</p>
                </li>
              </ul>
              <ul class="col-xs-12 col-sm-6 item-wrap-list wow zoomIn" animation-duration="1.5s" data-animated-delay="0.6">
                <li class="list-wrap-li">
                  <h4 class="item-title">1. Chi nhánh Công ty TNHH một thành viên thương mại dịch vụ Quốc Thái 2</h4>
                  <p class="item-descreption">Đường Khu Nhà 03 Căn - Khóm Thới Hòa - Nhà Bàn - Tịnh Biên - An Giang</p>
                  <p class="item-descreption">0939 631 907</p>
                </li>
              </ul>
              <ul class="col-xs-12 col-sm-6 item-wrap-list wow zoomIn" animation-duration="1.5s" data-animated-delay="0.7">
                <li class="list-wrap-li">
                  <h4 class="item-title">1. Chi nhánh Công ty TNHH một thành viên thương mại dịch vụ Quốc Thái 2</h4>
                  <p class="item-descreption">Đường Khu Nhà 03 Căn - Khóm Thới Hòa - Nhà Bàn - Tịnh Biên - An Giang</p>
                  <p class="item-descreption">0939 631 907</p>
                </li>
              </ul>
              <ul class="col-xs-12 col-sm-6 item-wrap-list wow zoomIn" animation-duration="1.5s" data-animated-delay="0.8">
                <li class="list-wrap-li">
                  <h4 class="item-title">1. Chi nhánh Công ty TNHH một thành viên thương mại dịch vụ Quốc Thái 2</h4>
                  <p class="item-descreption">Đường Khu Nhà 03 Căn - Khóm Thới Hòa - Nhà Bàn - Tịnh Biên - An Giang</p>
                  <p class="item-descreption">0939 631 907</p>
                </li>
              </ul>
              <ul class="col-xs-12 col-sm-6 item-wrap-list wow zoomIn" animation-duration="1.5s" data-animated-delay="0.9">
                <li class="list-wrap-li">
                  <h4 class="item-title">1. Chi nhánh Công ty TNHH một thành viên thương mại dịch vụ Quốc Thái 2</h4>
                  <p class="item-descreption">Đường Khu Nhà 03 Căn - Khóm Thới Hòa - Nhà Bàn - Tịnh Biên - An Giang</p>
                  <p class="item-descreption">0939 631 907</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>

    </main>

<?php
get_footer();
?>