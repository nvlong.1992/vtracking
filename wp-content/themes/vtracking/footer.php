<!-- include footer.html -->
<footer>
      <!--Footer-->
      <div class="footer-site ">
        <div class="container">
          <div class="row">
            <!--Footer Top-->
            <div class="footer-top ">
              <!--Footer Top Wrap-->
              <div class="footer-top-wrap wow fadeInLeftCustom" data-wow-duration="1.5s">
                <div class="col-xs-12 col-sm-6 btn-download">
                  <h2 class="title-footer">Tải và trải nghiệm phần mềm DMS.ONE trên thiết bị di động</h2>
                  <p class="item-descreption">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo</p>
                  <a class="item-icon-footer" href="http://apple.com/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/images/btn-apple.png" alt="">
                    </a>
                  <a class="item-icon-footer" href="https://play.google.com/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/images/btn-google-play.png" alt="">
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 image-footer-iphone wow fadeInUpCustom" data-wow-duration="1.5s" data-wow-delay="0.3s">
                  <img class="item-thin-images" src="<?php echo get_template_directory_uri(); ?>/images/photos/photo-mobile.png" alt="">
                </div>
              </div>
              <!--End Footer Top Wrap-->
            </div>
            <!--End Footer Top-->
            <!--Footer Bottom-->
          </div>
        </div>
        <div class="item-footer-bottom">
          <div class="container">
            <div class="row">
              <div class="footer-bottom">
                <h4 class="mg-bt-15">Tổng Công ty viễn thông Viettel</h4>
                <!--Footer Info-->
                <div class="footer__info">
                  <div class="footer__info-item mg-bt-15">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/icon-21.svg" alt="">
                    <p>Số 1 Giang Văn Minh, Kim Mã, Ba Đình, Hà Nội</p>
                  </div>
                  <div class="footer__info-item">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/icon-22.svg" alt="">
                    <p>18008000 (nhánh 1)</p>
                  </div>
                  <div class="footer__info-item">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/icon-23.svg" alt="">
                    <p>vtt_cskhdn@viettel.com.vn</p>
                  </div>
                </div>
                <!--Footer Info-->
                <!--Footer Contact-->
                <div class="footer__contact">
                  <div class="contact-left">
                    <ul class="list-unstyled">
                      <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-3.svg" alt=""></a></li>
                      <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-4.svg" alt=""></a></li>
                      <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-5.svg" alt=""></a></li>
                    </ul>
                    <div class="contact-btn">
                      <a class="item-contact" href="#modal-contact">
                            <span class="icon-lienhe"></span>
                            <span>Gửi liên hệ</span>
                          </a>
                    </div>
                  </div>
                  <div class="contact-right">
                    <p>Bản quyền thuộc về DMSONE.vn © 2018</p>
                  </div>
                </div>
                <!--End Footer Contact-->
              </div>
              <!--End Footer Bottom-->
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!--End Footer-->
  </div>
  <div id="login" class="zoom-anim-dialog mfp-hide wrap-sigin">
    <div class="signin-bg">
      <div class="signin-bg-wrapper">
        <form class="custom-form form-horizontal" name="login">
          <h2 class="form-title">Đăng nhập</h2>
          <div class="form-group input-form">
            <label class="label-content" for="usr">Tên đăng nhập</label>
            <input type="text" class="form-control" name="username" id="name" placeholder="Nhập tên đăng nhập">
          </div>
          <div class="form-group input-form">
            <label class="label-content" for="usr">
              Mật khẩu
            </label>
            <input type="password" class="form-control" name="password" id="password" value="" placeholder="Nhập mật khẩu">
          </div>
          <div class="label-content__link mg-bt-15">
            <a class="item-reset" href="#reset-pass">Đổi mật khẩu?</a>
            <a class="item-forgot" href="#forgot-pass">Quên mật khẩu?</a>
          </div>
          <div class="btn-button">
            <button name="submit" class="btn-basic" type="submit">Đăng nhập</button>
          </div>
        </form>
        <p class="item-signup">Bạn chưa có tài khoản? <a class="item-register" href="#modal_register">Đăng ký ngay</a></p>
      </div>
    </div>
  </div>
  <div id="reset-pass" class="zoom-anim-dialog mfp-hide wrap-sigin">
    <div class="signin-bg">
      <div class="signin-bg-wrapper">
        <h2 class="form-title item-title-register">Đổi mật khẩu</h2>
        <form class="custom-form form-register">
          <div class="form-group input-form ">
            <label class="label-content" for="name">Tên đăng nhập</label>
            <input type="text" class="form-control" placeholder="Nhập tên đăng nhập">
          </div>
          <div class="form-group input-form">
            <label class="label-content" for="password">Mật khẩu cũ</label>
            <input type="password" class="form-control" placeholder="Nhập...">
          </div>
          <div class="form-group input-form">
            <label class="label-content" for="new-password">Mật khẩu mới</label>
            <input type="password" class="form-control" placeholder="Nhập...">
          </div>
          <div class="form-group input-form">
            <label class="label-content" for="representative">Nhập lại mật khẩu mới</label>
            <input type="password" class="form-control" placeholder="Nhập...">
          </div>
          <div class="btn-button">
            <a class="btn-basic" href="">Đổi mật khẩu</a>
          </div>
        </form>
        <p class="item-signup">Bạn muốn đăng nhập tài khoản? <a class="item-login" href="#login">Đăng nhập ngay</a></p>
      </div>
    </div>
  </div>
  <div id="forgot-pass" class="zoom-anim-dialog mfp-hide wrap-sigin">
    <div class="signin-bg">
      <div class="signin-bg-wrapper">
        <form class="custom-form">
          <div class="signin-bg-wrapper">
            <form class="custom-form">
              <h2 class="form-title">Quên mật khẩu</h2>
              <div class="form-group input-form">
                <div class="item-content-daily">
                  <div class="item-select">
                    <div class="wrap-icon-select">
                      <select class="item-wrap-select" id="select_forgot" onchange="onchangeSelect()">
                        <option value="email">Lấy lại mật khẩu qua Email</option>
                        <option value="sdt">Lấy lại mật khẩu qua Số điện thoại</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group input-form hidden-input-email active-input">
                <label class="label-content" for="name">Email</label>
                <input type="text" class="form-control" placeholder="Địa chỉ Email">
              </div>
              <div class="form-group input-form hidden-input-sdt">
                <label class="label-content" for="name">Số điện thoại</label>
                <input type="text" class="form-control" placeholder="Số điện thoại">
              </div>
              <div class="btn-button">
                <a class="btn-basic" href="">Gửi yêu cầu Đặt lại mật khẩu</a>
              </div>
            </form>
          </div>
        </form>
        <p class="item-signup">Bạn muốn đăng nhập tài khoản? <a class="item-login" href="#login">Đăng nhập ngay</a></p>
      </div>
    </div>
  </div>
  <div id="modal_register" class="zoom-anim-dialog mfp-hide wrap-sigin">
    <div class="signin-bg">
      <div class="signin-bg-wrapper">
        <h2 class="form-title item-title-register">Đăng ký</h2>
        <form class="custom-form form-register">
          <h4 class="headding-register">Để đăng ký dịch vụ Giám sát hành trình V-Tracking, vui lòng liên hệ:</h4>
          <div class="item-wrap-content">
            <p class="wrap-content-register">Các <span class="item-thin-register">cửa hàng Viettel trên toàn quốc</span></p>
            <p class="wrap-content-register">Các <span class="item-thin-register">đại lý, cửa hàng xe máy được Viettel ủy quyền</span></p>
            <p class="wrap-content-register"> Tổng đài <span class="item-thin-register">18008000 nhánh 1</span> (miễn phí)</p>
          </div>
        </form>
        <p class="item-signup">Bạn đã có tài khoản? <a class="item-login" href="#login">Đăng nhập ngay</a></p>
      </div>
    </div>
  </div>
  <div id="modal-contact" class="zoom-anim-dialog mfp-hide wrap-sigin">
    <div class="signin-bg">
      <div class="signin-bg-wrapper">
        <h2 class="form-title item-title-register">Liên hệ</h2>
        <form class="custom-form form-contact">
          <h4 class="heading-contact">Cho chúng tôi biết nếu bạn có bất kỳ góp ý hay phản hồi nào</h4>
          <div class="form-group input-form item-input">
            <label class="label-content" for="name">Họ và tên</label>
            <input type="text" class="form-control" placeholder="Nhập họ và tên">
          </div>
          <div class="form-group input-form item-input">
            <label class="label-content" for="email">Địa chỉ email</label>
            <input type="text" class="form-control" placeholder="Nhập email">
          </div>
          <div class="form-group input-form item-input">
            <label class="label-content" for="address">Điện thoại</label>
            <input type="number" class="form-control" placeholder="Nhập số điện thoại">
          </div>
          <div class="form-group input-form">
            <label class="label-content" for="representative">Nội dung liên hệ</label>
            <textarea id="form_message" name="message" class="form-control message-form" placeholder="Nhập nội dung" rows="4" required="required" data-error="Please, leave us a message."></textarea>
          </div>
          <div class="btn-button">
            <a class="btn-basic btn-contact" href="#">Gửi liên hệ</a>
          </div>
        </form>
      </div>
    </div>
  </div>
  <p class="cd-top">
    <span class="icon-down"></span>
  </p>
  <div class="messages wow fadeInDown" data-wow-duration="1s">
    <span class="item-message">Đăng nhập thành công</span>
  </div>
  <div class="item-loadding">
    <div class="container-loader">
      <div class="loader">
        <div class="loader--dot"></div>
        <div class="loader--dot"></div>
        <div class="loader--dot"></div>
        <div class="loader--dot"></div>
        <div class="loader--dot"></div>
        <div class="loader--dot"></div>
        <div class="loader--text"></div>
      </div>
    </div>
  </div>
  <!-- include scripts.html -->
  <script src="scripts/vendor-f0d4904e9e.min.js"></script>
  <script src="scripts/script-9319910527.min.js"></script>
</body>