<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>V_tracking</title>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/vendor-388dcabdc0.min.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style-aef91716b6.min.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/animate.min.css" type="text/css" media="screen" />

  <script src="<?php bloginfo('template_url'); ?>/js/jquery-3.5.1.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/wow.min.js"></script>
  <script type="text/javascript">
    new WOW().init();
  </script>
  <link href="<?php echo get_bloginfo( 'template_directory' );?>/blog.css" rel="stylesheet">

  <?php wp_head();?>
</head>

<body>
  <div class="body-wrap">
    <!-- include header.html -->
    <header class="header-site">
      <div class="item-header-top">
        <div class="container">
          <div class="header-top">
            <!--Header Top Left-->
            <div class="header-top__left">
              <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-2.svg" alt="">
                  Email: <span> bikesmart@dinhvixedien.com</span>
              </a>
              <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-1.svg" alt="">
                  Hotline kinh doanh: <span>024.6666.89.89</span>
              </a>
            </div>
            <!--End Header Top Left-->
            <!--Header Top Right-->
            <div class="header-top__right ">
              <ul class="header__social list-unstyled">
                <li>
                  <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-3.svg" alt=""></a>
                </li>
                <li>
                  <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-4.svg" alt=""></a>
                </li>
                <li>
                  <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-5.svg" alt=""></a>
                </li>
              </ul>
              <ul class="header__language list-unstyled">
                <li class="active">
                  <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/flag-en.png" alt=""></a>
                </li>
                <li>
                  <a href=""><img class="active-language" src="<?php echo get_template_directory_uri(); ?>/images/flag-vn.png" alt=""></a>
                </li>
              </ul>
            </div>
            <!--End Header Top Right-->
          </div>
        </div>
      </div>
      <!--End Header Top-->
      <!--Header Bottom-->
      <div class="container">
        <div class="header-bottom">
          <div class="header__logo">
            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/images/logo-bike.jpg" alt=""></a>
            <!-- <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/images/logo-vtracking.png" alt=""></a> -->
          </div>
          <div class="header__menu__btn">
            <span class="content-btn"></span>
          </div>
          <div class="header__social__mobile">
            <div class="header__social__mobile--left">
              <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-3.svg" alt=""></a>
              <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-4.svg" alt=""></a>
              <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-5.svg" alt=""></a>

            </div>
            <div class="header__social__mobile--right">
              <a href="" class="active"><img src="<?php echo get_template_directory_uri(); ?>/images/flag-en.png" alt=""></a>
              <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/flag-vn.png" alt=""></a>
            </div>
          </div>
          <div class="wrap-flex-right">
            <nav class="header__menu">
              <?php
                wp_nav_menu( array( 
                  'theme_location' => 'my-menu', 
                  'menu_class' => 'main-menu list-unstyled',
                  'menu_id' => 'myNavbar')); 
              ?>
              <!-- <ul class="main-menu list-unstyled" id="myNavbar">
                <li><a class="item-nav" href="#tin-nang">Tính năng</a></li>
                <li><a class="item-nav" href="#mo-hinh">Mô hình</a></li>
                <li><a class="item-nav" href="#phuong-thuc">Phương thức</a></li>
                <li><a class="item-nav" href="#doi_tuong">Đối tượng sử dụng</a></li>
                <li><a class="item-nav" href="#dich-vu">Phí Dịch vụ</a></li>
                <li><a class="item-nav" href="#dai-ly">Đại lý</a></li>
              </ul> -->
            </nav>
            <div class="header__btn">
              <a class="item-login" href="#login">
                <span class="icon-dangnhap" ></span>
                <span>Đăng nhập</span>
              </a>
            </div>
          </div>
          <div class="header__info__mobile">
            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-2.svg" alt="">vtt_cskhdn@viettel.com.vn</a>
            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-1.svg" alt="">18008000</a>
          </div>
        </div>
      </div>
      <!--End Header Bottom-->
    </header>